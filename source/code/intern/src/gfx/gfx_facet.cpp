#include "gfx/gfx_facet.h"

namespace Data
{
    CFacet::CFacet()
        : m_Facets{ }
        , m_pMetaFacet{ nullptr }
    {
    }

    CFacet::~CFacet()
    {
    }

    CFacet::BID CFacet::GetID()
    {
        return m_ID;
    }

    void CFacet::SetID(CFacet::BID _ID)
    {
        m_ID = _ID;
    }

    void CFacet::Initialize(CMetaFacet& _rMetaFacet)
    {
        m_pMetaFacet = &_rMetaFacet;
    }
}
