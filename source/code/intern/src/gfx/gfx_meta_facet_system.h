#pragma once

#include "core/core_singleton.h"
#include "core/core_id_manager.h"
#include "core/core_item_manager.h"
#include "gfx/gfx_meta_facet.h"

#include <vector>
#include <string>

namespace Data
{
    class CMetaFacetSystem : public Core::CSingleton<CMetaFacetSystem>
    {
        public:

            CMetaEntity& CreateMetaFacet(const std::string& _rName);
            void DestroyMetaFacet(CMetaFacet& _rMetaFacet);

        private:

            CMetaFacetSystem();
           ~CMetaFacetSystem();

        private:

            Core::CIDManager               m_IDManager;
            Core::CItemManager<CMetaFacet> m_ItemManager;
            std::list<CMetaFacet*>         m_MetaFacets;
    };
}
