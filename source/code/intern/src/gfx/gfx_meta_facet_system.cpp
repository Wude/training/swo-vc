#include "gfx/gfx_meta_facet_system.h"

#include <algorithm>

namespace Data
{
    CMetaFacetSystem::CMetaFacetSystem()
        : m_MetaEntities{}
    {
    }

    CMetaFacetSystem::~CMetaFacetSystem()
    {
    }

    CMetaFacet& CMetaFacetSystem::CreateMetaFacet(const std::string& _rName)
    {
        Core::CIDManager::BID ID = m_IDManager.Register(_rName);
        CMetaFacet& rMetaFacet = m_ItemManager.CreateItem(ID);
        rMetaFacet.SetID(ID);
        m_MetaEntities.push_back(&rMetaFacet);
        return rMetaFacet;
    }

    void CMetaFacetSystem::DestroyMetaFacet(CMetaFacet& _rMetaFacet)
    {
        m_MetaEntities.remove(&_rMetaFacet);
    }
}
