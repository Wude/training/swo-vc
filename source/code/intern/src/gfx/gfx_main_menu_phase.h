#pragma once

#include "core/core_singleton.h"

#include "SFML/Graphics.hpp"

namespace Gfx
{
    class CMainMenuPhase : public Core::CSingleton<CMainMenuPhase>
    {
        public:

            void OnEnter(sf::RenderWindow& _rWindow);
            void OnLeave();
            void OnRun();
    };
}
