#pragma once

#include "core/core_singleton.h"

namespace Gfx
{
    class CFinalizePhase : public Core::CSingleton<CFinalizePhase>
    {
        public:

            void OnEnter();
            void OnLeave();
            void OnRun();
    };
}
