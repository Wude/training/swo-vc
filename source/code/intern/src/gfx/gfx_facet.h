#pragma once

#include "core/core_id_manager.h"
#include "gfx/gfx_meta_facet.h"

namespace Data
{
    class CFacetSystem;
}

namespace Data
{
    class CFacet
    {
        public:

            using BID = Core::CIDManager::BID;

        public:

            CFacet::BID GetID();

        public:

            CFacet();
           ~CFacet();

        private:

            friend class CFacetSystem;

        private:

            void Initialize(CMetaFacet& _rMetaFacet);
            void SetID(CFacet::BID _ID);

        private:

            CFacet::BID m_ID;
            CMetaFacet* m_pMetaFacet;
    };
}
