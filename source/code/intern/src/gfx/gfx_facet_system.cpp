#include "gfx/gfx_facet_system.h"

#include <algorithm>

namespace Data
{
    CFacetSystem::CFacetSystem()
        : m_Facets{}
    {
    }

    CFacetSystem::~CFacetSystem()
    {
    }

    CFacet& CFacetSystem::CreateFacet(const std::string& _rName)
    {
        Core::CIDManager::BID ID = m_IDManager.Register(_rName);
        CFacet& rFacet = m_ItemManager.CreateItem(ID);
        rFacet.SetID(ID);
        m_Facets.push_back(&rFacet);
        return rFacet;
    }

    void CFacetSystem::DestroyFacet(CFacet& _rFacet)
    {
        m_Facets.remove(&_rFacet);
    }
}
