#include "gfx/gfx_play_phase.h"

namespace Gfx
{
    CPlayPhase::CPlayPhase()
        : m_pWindow{ nullptr }
    {
    }

    void CPlayPhase::OnEnter(sf::RenderWindow& _rWindow)
    {
        m_pWindow = &_rWindow;
    }

    void CPlayPhase::OnRun()
    {
    }

    void CPlayPhase::OnLeave()
    {
        m_pWindow = nullptr;
    }
}
