#include "gfx/gfx_meta_facet.h"

namespace Data
{
    CMetaFacet::CMetaFacet()
        : m_Facets{ }
        , m_Category{ SEntityCategory::Enum::Undefined }
    {
    }

    CMetaFacet::~CMetaFacet()
    {
    }

    CMetaFacet::BID CMetaFacet::GetID()
    {
        return m_ID;
    }

    void CMetaFacet::SetID(CMetaFacet::BID _ID)
    {
        m_ID = _ID;
    }
}
