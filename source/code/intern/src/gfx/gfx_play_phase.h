#pragma once

#include "core/core_singleton.h"

#include "SFML/Graphics.hpp"

namespace Gfx
{
    class CPlayPhase : public Core::CSingleton<CPlayPhase>
    {
        public:

            void OnEnter(sf::RenderWindow& _rWindow);
            void OnLeave();
            void OnRun();

        public:

            CPlayPhase();

        private:

            sf::RenderWindow* m_pWindow;
    };
}
