#pragma once

#include "core/core_id_manager.h"

namespace Data
{
    class CMetaFacetSystem;
}

namespace Data
{
    class CMetaFacet
    {
        public:

            using BID = Core::CIDManager::BID;

        public:

            CMetaFacet::BID GetID();

        public:

            CMetaFacet();
           ~CMetaFacet();

        private:

            friend class CMetaFacetSystem;

        private:

            void SetID(CMetaFacet::BID _ID);

        private:

            CMetaFacet::BID m_ID;
    };
}
