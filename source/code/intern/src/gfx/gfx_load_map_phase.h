#pragma once

#include "core/core_singleton.h"

#include "SFML/Graphics.hpp"

namespace Gfx
{
    class CLoadMapPhase : public Core::CSingleton<CLoadMapPhase>
    {
        public:

            void OnEnter(sf::RenderWindow& _rWindow);
            void OnLeave();
            void OnRun();
    };
}
