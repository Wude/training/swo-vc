#pragma once

#include "core/core_singleton.h"

namespace Gfx
{
    class CInitializePhase : public Core::CSingleton<CInitializePhase>
    {
        public:

            void OnEnter();
            void OnLeave();
            void OnRun();
    };
}
