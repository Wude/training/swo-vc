#pragma once

#include "core/core_singleton.h"
#include "core/core_id_manager.h"
#include "core/core_item_manager.h"
#include "gfx/gfx_facet.h"

#include <vector>
#include <string>

namespace Data
{
    class CFacetSystem : public Core::CSingleton<CFacetSystem>
    {
        public:

            CFacet& CreateFacet(const std::string& _rName);
            void DestroyFacet(CFacet& _rFacet);

        private:

            CFacetSystem();
           ~CFacetSystem();

        private:

            Core::CIDManager           m_IDManager;
            Core::CItemManager<CFacet> m_ItemManager;
            std::list<CFacet*>         m_Facets;
    };
}
