#pragma once

#include "core/core_singleton.h"

namespace Logic
{
    class CLoadMapPhase : public Core::CSingleton<CLoadMapPhase>
    {
        public:

            void OnEnter();
            void OnLeave();
            void OnRun();
    };
}
