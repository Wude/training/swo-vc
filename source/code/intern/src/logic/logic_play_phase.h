#pragma once

#include "core/core_singleton.h"

namespace Logic
{
    class CPlayPhase : public Core::CSingleton<CPlayPhase>
    {
        public:

            void OnEnter();
            void OnLeave();
            void OnRun();
    };
}
