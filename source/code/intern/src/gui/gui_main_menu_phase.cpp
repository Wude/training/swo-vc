#include "gui/gui_main_menu_phase.h"
#include "core/core_user_event.h"
#include "data/data_user_event_system.h"

namespace Gui
{
    CMainMenuPhase::CMainMenuPhase()
        : m_Action{ CMainMenuPhase::EAction::None }
    {
    }

    void CMainMenuPhase::OnEnter()
    {
        m_Action = CMainMenuPhase::EAction::None;
        Data::CUserEventSystem::GetInstance().Register(*this);
    }

    void CMainMenuPhase::OnLeave()
    {
        Data::CUserEventSystem::GetInstance().Unregister(*this);
    }

    CMainMenuPhase::EAction CMainMenuPhase::OnRun()
    {
        return m_Action;
    }

    void CMainMenuPhase::OnEvent(const Core::CUserEvent& _rEvent)
    {
        switch (_rEvent.GetKey())
        {
        case Core::CUserEvent::EKey::KeyEscape:
            if (_rEvent.GetAction() == Core::CUserEvent::EAction::KeyPressed)
            {
                m_Action = CMainMenuPhase::EAction::Close;
            }
            break;
        }
    }
}
