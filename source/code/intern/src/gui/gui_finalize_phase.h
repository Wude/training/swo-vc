#pragma once

#include "core/core_singleton.h"

namespace Gui
{
    class CFinalizePhase : public Core::CSingleton<CFinalizePhase>
    {
        public:

            void OnEnter();
            void OnLeave();
            void OnRun();
    };
}
