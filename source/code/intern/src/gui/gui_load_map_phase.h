#pragma once

#include "core/core_singleton.h"

namespace Gui
{
    class CLoadMapPhase : public Core::CSingleton<CLoadMapPhase>
    {
        public:

            void OnEnter();
            void OnLeave();
            void OnRun();
    };
}
