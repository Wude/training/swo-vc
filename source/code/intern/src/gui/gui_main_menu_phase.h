#pragma once

#include "core/core_singleton.h"
#include "data/data_event_listener.h"
#include "core/core_user_event.h"

namespace Gui
{
    class CMainMenuPhase : public Core::CSingleton<CMainMenuPhase>
                         , public Data::CEventListener<Core::CUserEvent>
    {
        public:

            enum EAction : int
            {
                None,
                Close,
            };

        public:

            void OnEnter();
            void OnLeave();
            CMainMenuPhase::EAction OnRun();

        public:

            void OnEvent(const Core::CUserEvent& _rEvent);

        private:

            CMainMenuPhase();

        private:

            template <class T> friend class Core::CSingleton;

        private:

            CMainMenuPhase::EAction m_Action;
    };
}
