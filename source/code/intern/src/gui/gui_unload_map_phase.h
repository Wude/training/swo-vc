#pragma once

#include "core/core_singleton.h"

namespace Gui
{
    class CUnloadMapPhase : public Core::CSingleton<CUnloadMapPhase>
    {
        public:

            void OnEnter();
            void OnLeave();
            void OnRun();
    };
}
