
#pragma once

#include "core/core_aabb2.h"
#include "core/core_aabb3.h"

namespace Core
{
    typedef CAABB2<signed char>      CAABB2SChar;
    typedef CAABB2<unsigned char>    CAABB2UChar;
    typedef CAABB2<signed short>     CAABB2SShort;
    typedef CAABB2<unsigned short>   CAABB2UShort;
    typedef CAABB2<signed int>       CAABB2SInt;
    typedef CAABB2<unsigned int>     CAABB2UInt;
    typedef CAABB2<signed long>      CAABB2SLong;
    typedef CAABB2<unsigned long>    CAABB2ULong;
    typedef CAABB2<signed long long> CAABB2SLongLong;
    typedef CAABB2<unsigned long>    CAABB2ULongLong;
    typedef CAABB2<float>            CAABB2Float;
    typedef CAABB2<double>           CAABB2Double;
} // namespace Core

namespace Core
{
    typedef CAABB3<signed char>      CAABB3SChar;
    typedef CAABB3<unsigned char>    CAABB3UChar;
    typedef CAABB3<signed short>     CAABB3SShort;
    typedef CAABB3<unsigned short>   CAABB3UShort;
    typedef CAABB3<signed int>       CAABB3SInt;
    typedef CAABB3<unsigned int>     CAABB3UInt;
    typedef CAABB3<signed long>      CAABB3SLong;
    typedef CAABB3<unsigned long>    CAABB3ULong;
    typedef CAABB3<signed long long> CAABB3SLongLong;
    typedef CAABB3<unsigned long>    CAABB3ULongLong;
    typedef CAABB3<float>            CAABB3Float;
    typedef CAABB3<double>           CAABB3Double;
} // namespace Core

