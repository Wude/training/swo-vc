
#pragma once

#include "core/core_matrix2x2.h"
#include "core/core_matrix3x3.h"
#include "core/core_matrix4x4.h"

namespace Core
{
    typedef CMatrix2x2<signed char>      SChar2x2;
    typedef CMatrix2x2<unsigned char>    UChar2x2;
    typedef CMatrix2x2<signed short>     SShort2x2;
    typedef CMatrix2x2<unsigned short>   UShort2x2;
    typedef CMatrix2x2<signed int>       SInt2x2;
    typedef CMatrix2x2<unsigned int>     UInt2x2;
    typedef CMatrix2x2<signed long>      SLong2x2;
    typedef CMatrix2x2<unsigned long>    ULong2x2;
    typedef CMatrix2x2<signed long long> SLongLong2x2;
    typedef CMatrix2x2<unsigned long>    ULongLong2x2;
    typedef CMatrix2x2<float>            Float2x2;
    typedef CMatrix2x2<double>           Double2x2;
} // namespace Core

namespace Core
{
    typedef CMatrix3x3<signed char>      SChar3x3;
    typedef CMatrix3x3<unsigned char>    UChar3x3;
    typedef CMatrix3x3<signed short>     SShort3x3;
    typedef CMatrix3x3<unsigned short>   UShort3x3;
    typedef CMatrix3x3<signed int>       SInt3x3;
    typedef CMatrix3x3<unsigned int>     UInt3x3;
    typedef CMatrix3x3<signed long>      SLong3x3;
    typedef CMatrix3x3<unsigned long>    ULong3x3;
    typedef CMatrix3x3<signed long long> SLongLong3x3;
    typedef CMatrix3x3<unsigned long>    ULongLong3x3;
    typedef CMatrix3x3<float>            Float3x3;
    typedef CMatrix3x3<double>           Double3x3;
} // namespace Core

namespace Core
{
    typedef CMatrix4x4<signed char>      SChar4x4;
    typedef CMatrix4x4<unsigned char>    UChar4x4;
    typedef CMatrix4x4<signed short>     SShort4x4;
    typedef CMatrix4x4<unsigned short>   UShort4x4;
    typedef CMatrix4x4<signed int>       SInt4x4;
    typedef CMatrix4x4<unsigned int>     UInt4x4;
    typedef CMatrix4x4<signed long>      SLong4x4;
    typedef CMatrix4x4<unsigned long>    ULong4x4;
    typedef CMatrix4x4<signed long long> SLongLong4x4;
    typedef CMatrix4x4<unsigned long>    ULongLong4x4;
    typedef CMatrix4x4<float>            Float4x4;
    typedef CMatrix4x4<double>           Double4x4;
} // namespace Core
