#pragma once

#include "core/core_uncopyable.h"

namespace Core
{
    template <class T>
    class CSingleton : private CUncopyable
    {
        public:
            static T &GetInstance() {
                static T instance;
                return instance;
            }
    };
} // namespace Core
