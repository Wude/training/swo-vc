
#pragma once

#include "core/core_vector2.h"
#include "core/core_vector3.h"
#include "core/core_vector4.h"

namespace Core
{
    typedef Core::CVector2<signed short>   SShort2;
    typedef Core::CVector2<unsigned short> UShort2;
    typedef Core::CVector2<signed int>     SInt2;
    typedef Core::CVector2<unsigned int>   UInt2;
    typedef Core::CVector2<float>          Float2;
} // namespace Core

namespace Core
{
	typedef Core::CVector3<signed short>   SShort3;
	typedef Core::CVector3<unsigned short> UShort3;
	typedef Core::CVector3<signed int>     SInt3;
	typedef Core::CVector3<unsigned int>   UInt3;
	typedef Core::CVector3<float>          Float3;
} // namespace Core

namespace Core
{
	typedef Core::CVector4<signed short>   SShort4;
	typedef Core::CVector4<unsigned short> UShort4;
	typedef Core::CVector4<signed int>     SInt4;
	typedef Core::CVector4<unsigned int>   UInt4;
	typedef Core::CVector4<float>          Float4;
} // namespace Core