#pragma once

#include "core/core_singleton.h"
#include "game/game_phase.h"

namespace Game
{
    class CMainMenuPhase : public CPhase, public Core::CSingleton<CMainMenuPhase>
    {
        protected:

            void InternalOnEnter() override;
            void InternalOnLeave() override;
            CPhase::Enum InternalOnRun() override;

        private:

            template <class T> friend class Core::CSingleton;

        private:

            CMainMenuPhase();
            ~CMainMenuPhase() override;
    };
}
