#pragma once

#include "core/core_singleton.h"
#include "game/game_phase.h"

#include "SFML/Graphics.hpp"

namespace Game
{
    class CApplication : public Core::CSingleton<CApplication>
    {
        public:

            sf::RenderWindow& GetWindow();

            void Startup(unsigned int width, unsigned int height);
            void Shutdown();
            void Run();

        private:

            bool RunPhases();

        private:

            template <class T> friend class Core::CSingleton;

            static constexpr int s_NumberOfPhases = CPhase::Enum::NumberOfPhases;

        private:

            CApplication();
            ~CApplication();

        private:

            CPhase::Enum  m_CurrentPhase;
            CPhase::Enum  m_NextPhase;
            CPhase* m_pPhases[s_NumberOfPhases];

            sf::RenderWindow m_Window;

    };
}
