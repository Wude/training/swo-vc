#include "game/game_finalize_phase.h"
#include "gui/gui_finalize_phase.h"
#include "gfx/gfx_finalize_phase.h"
#include "logic/logic_finalize_phase.h"
#include "data/data_finalize_phase.h"

namespace Game
{
    CFinalizePhase::CFinalizePhase()
    {
    }

    CFinalizePhase::~CFinalizePhase()
    {
    }

    void CFinalizePhase::InternalOnEnter()
    {
        Gui::CFinalizePhase::GetInstance().OnEnter();
        Gfx::CFinalizePhase::GetInstance().OnEnter();
        Logic::CFinalizePhase::GetInstance().OnEnter();
        Data::CFinalizePhase::GetInstance().OnEnter();
    }

    void CFinalizePhase::InternalOnLeave()
    {
        Data::CFinalizePhase::GetInstance().OnLeave();
        Logic::CFinalizePhase::GetInstance().OnLeave();
        Gfx::CFinalizePhase::GetInstance().OnLeave();
        Gui::CFinalizePhase::GetInstance().OnLeave();
    }

    CPhase::Enum  CFinalizePhase::InternalOnRun()
    {
        Gui::CFinalizePhase::GetInstance().OnRun();
        Gfx::CFinalizePhase::GetInstance().OnRun();
        Logic::CFinalizePhase::GetInstance().OnRun();
        Data::CFinalizePhase::GetInstance().OnRun();

        return CPhase::Enum::DeadPhase;
    }
}
