#pragma once

#include "core/core_singleton.h"
#include "game/game_phase.h"

namespace Game
{
    class CPlayPhase : public CPhase, public Core::CSingleton<CPlayPhase>
    {
        protected:

            void InternalOnEnter() override;
            void InternalOnLeave() override;
            CPhase::Enum InternalOnRun() override;

        private:

            template <class T> friend class Core::CSingleton;

        private:

            CPlayPhase();
            ~CPlayPhase() override;
    };
}
