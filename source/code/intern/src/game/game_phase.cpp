#include "game/game_phase.h"

namespace Game
{
    CPhase::~CPhase()
    {
    }

    void CPhase::OnEnter()
    {
        InternalOnEnter();
    }

    void CPhase::OnLeave()
    {
        InternalOnLeave();
    }

    CPhase::Enum CPhase::OnRun()
    {
        return InternalOnRun();
    }
}
