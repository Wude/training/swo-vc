#include "game/game_application.h"
#include "game/game_load_map_phase.h"
#include "data/data_load_map_phase.h"
#include "logic/logic_load_map_phase.h"
#include "gfx/gfx_load_map_phase.h"
#include "gui/gui_load_map_phase.h"

namespace Game
{
    CLoadMapPhase::CLoadMapPhase()
    {
    }

    CLoadMapPhase::~CLoadMapPhase()
    {
    }

    void CLoadMapPhase::InternalOnEnter()
    {
        Data::CLoadMapPhase::GetInstance().OnEnter();
        Logic::CLoadMapPhase::GetInstance().OnEnter();
        Gfx::CLoadMapPhase::GetInstance().OnEnter(CApplication::GetInstance().GetWindow());
        Gui::CLoadMapPhase::GetInstance().OnEnter();
    }

    void CLoadMapPhase::InternalOnLeave()
    {
        Gui::CLoadMapPhase::GetInstance().OnLeave();
        Gfx::CLoadMapPhase::GetInstance().OnLeave();
        Logic::CLoadMapPhase::GetInstance().OnLeave();
        Data::CLoadMapPhase::GetInstance().OnLeave();
    }

    CPhase::Enum CLoadMapPhase::InternalOnRun()
    {
        Data::CLoadMapPhase::GetInstance().OnRun();
        Logic::CLoadMapPhase::GetInstance().OnRun();
        Gfx::CLoadMapPhase::GetInstance().OnRun();
        Gui::CLoadMapPhase::GetInstance().OnRun();

        return CPhase::Enum::PlayPhase;
    }
}
