#include "game/game_application.h"
#include "game/game_initialize_phase.h"
#include "game/game_finalize_phase.h"
#include "game/game_main_menu_phase.h"
#include "game/game_load_map_phase.h"
#include "game/game_play_phase.h"
#include "game/game_unload_map_phase.h"
#include "data/data_user_event_system.h"
#include "core/core_user_event.h"

#include <iostream>

namespace Game
{
    CApplication::CApplication()
        : m_CurrentPhase(CPhase::Enum::DeadPhase)
        , m_NextPhase(CPhase::Enum::DeadPhase)
    {
        m_pPhases[CPhase::Enum::InitializePhase]  = &CInitializePhase::GetInstance();
        m_pPhases[CPhase::Enum::MainMenuPhase]    = &CMainMenuPhase  ::GetInstance();
        m_pPhases[CPhase::Enum::LoadMapPhase]     = &CLoadMapPhase   ::GetInstance();
        m_pPhases[CPhase::Enum::PlayPhase]        = &CPlayPhase      ::GetInstance();
        m_pPhases[CPhase::Enum::UnloadMapPhase]   = &CUnloadMapPhase ::GetInstance();
        m_pPhases[CPhase::Enum::FinalizePhase]    = &CFinalizePhase  ::GetInstance();
    }

    CApplication::~CApplication()
    {
    }

    sf::RenderWindow& CApplication::GetWindow()
    {
        return m_Window;
    }

    void CApplication::Startup(unsigned int width, unsigned int height)
    {
        m_Window.create(sf::VideoMode(width, height), "SWO: Spielbasis");

        m_CurrentPhase = CPhase::Enum::DeadPhase;
        m_NextPhase    = CPhase::Enum::InitializePhase;
    }

    void CApplication::Shutdown()
    {
        m_Window.close();
    }

    void CApplication::Run()
    {
        while (RunPhases())
        {
            // std::cout << "CApplication::Run() -> " << m_Window.isOpen() << std::endl;

            sf::Event Event;

            while (m_Window.pollEvent(Event))
            {
                switch (Event.type)
                {
                case sf::Event::Closed:
                    // std::cout << "CApplication::Run() -> sf::Event::Closed" << std::endl;
                    Data::CUserEventSystem::GetInstance().FireEvent(Core::CUserEvent(Core::CUserEvent::EAction::KeyPressed, Core::CUserEvent::EKey::KeyEscape));
                    break;

                case sf::Event::KeyPressed:
                    switch (Event.key.code) {
                    case sf::Keyboard::Escape:
                        Data::CUserEventSystem::GetInstance().FireEvent(Core::CUserEvent(Core::CUserEvent::EAction::KeyPressed, Core::CUserEvent::EKey::KeyEscape));
                        break;
                    case sf::Keyboard::Enter:
                        Data::CUserEventSystem::GetInstance().FireEvent(Core::CUserEvent(Core::CUserEvent::EAction::KeyPressed, Core::CUserEvent::EKey::KeyEnter));
                        break;
                    case sf::Keyboard::Up:
                        Data::CUserEventSystem::GetInstance().FireEvent(Core::CUserEvent(Core::CUserEvent::EAction::KeyPressed, Core::CUserEvent::EKey::KeyUp));
                        break;
                    case sf::Keyboard::Down:
                        Data::CUserEventSystem::GetInstance().FireEvent(Core::CUserEvent(Core::CUserEvent::EAction::KeyPressed, Core::CUserEvent::EKey::KeyDown));
                        break;
                    case sf::Keyboard::Left:
                        Data::CUserEventSystem::GetInstance().FireEvent(Core::CUserEvent(Core::CUserEvent::EAction::KeyPressed, Core::CUserEvent::EKey::KeyLeft));
                        break;
                    case sf::Keyboard::Right:
                        Data::CUserEventSystem::GetInstance().FireEvent(Core::CUserEvent(Core::CUserEvent::EAction::KeyPressed, Core::CUserEvent::EKey::KeyRight));
                        break;
                    }
                    break;
                }
            }
        }
    }

    bool CApplication::RunPhases()
    {
        if (m_CurrentPhase != m_NextPhase)
        {
            if (m_CurrentPhase != CPhase::Enum::DeadPhase)
            {
                m_pPhases[m_CurrentPhase]->OnLeave();
            }

            m_CurrentPhase = m_NextPhase;

            if (m_CurrentPhase != CPhase::DeadPhase)
            {
                m_pPhases[m_CurrentPhase]->OnEnter();
            }
        }

        if (m_CurrentPhase != CPhase::Enum::DeadPhase)
        {
            m_NextPhase = static_cast<CPhase::Enum>(m_pPhases[m_CurrentPhase]->OnRun());
            // std::cout << "CApplication::RunPhases() -> " << m_NextPhase << std::endl;
            return true;
        }
        return false;
    }
}
