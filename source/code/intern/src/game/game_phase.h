#pragma once

namespace Game
{
    class CPhase
    {
        public:

            enum Enum : int
            {
                InitializePhase,
                MainMenuPhase,
                LoadMapPhase,
                PlayPhase,
                UnloadMapPhase,
                FinalizePhase,
                NumberOfPhases,
                DeadPhase = -1
            };

        public:

            void OnEnter();
            void OnLeave();
            CPhase::Enum OnRun();

        protected:

            virtual ~CPhase() = 0;

        protected:

            virtual void InternalOnEnter() = 0;
            virtual void InternalOnLeave() = 0;
            virtual CPhase::Enum InternalOnRun() = 0;
    };
}
