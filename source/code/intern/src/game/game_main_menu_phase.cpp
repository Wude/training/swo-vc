#include "game/game_application.h"
#include "game/game_main_menu_phase.h"
#include "gfx/gfx_main_menu_phase.h"
#include "gui/gui_main_menu_phase.h"

#include <iostream>

namespace Game
{
    CMainMenuPhase::CMainMenuPhase()
    {
    }

    CMainMenuPhase::~CMainMenuPhase()
    {
    }

    void CMainMenuPhase::InternalOnEnter()
    {
        Gfx::CMainMenuPhase::GetInstance().OnEnter(CApplication::GetInstance().GetWindow());
        Gui::CMainMenuPhase::GetInstance().OnEnter();
    }

    void CMainMenuPhase::InternalOnLeave()
    {
        Gui::CMainMenuPhase::GetInstance().OnLeave();
        Gfx::CMainMenuPhase::GetInstance().OnLeave();
    }

    CPhase::Enum CMainMenuPhase::InternalOnRun()
    {
        Gfx::CMainMenuPhase::GetInstance().OnRun();
        switch (Gui::CMainMenuPhase::GetInstance().OnRun())
        {
        case Gui::CMainMenuPhase::EAction::Close:
            // std::cout << "CMainMenuPhase::InternalOnRun() -> CPhase::Enum::FinalizePhase" << std::endl;
            return CPhase::Enum::FinalizePhase;
        case Gui::CMainMenuPhase::EAction::None:
        default:
            break;
        }

        // std::cout << "CMainMenuPhase::InternalOnRun() -> CPhase::Enum::MainMenuPhase" << std::endl;
        return CPhase::Enum::MainMenuPhase;
    }
}
