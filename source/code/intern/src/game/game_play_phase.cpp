#include "game/game_application.h"
#include "game/game_play_phase.h"
#include "logic/logic_play_phase.h"
#include "gfx/gfx_play_phase.h"
#include "gui/gui_play_phase.h"

namespace Game
{
    CPlayPhase::CPlayPhase()
    {
    }

    CPlayPhase::~CPlayPhase()
    {
    }

    void CPlayPhase::InternalOnEnter()
    {
        Logic::CPlayPhase::GetInstance().OnEnter();
        Gfx::CPlayPhase::GetInstance().OnEnter(CApplication::GetInstance().GetWindow());
        Gui::CPlayPhase::GetInstance().OnEnter();
    }

    void CPlayPhase::InternalOnLeave()
    {
        Gui::CPlayPhase::GetInstance().OnLeave();
        Gfx::CPlayPhase::GetInstance().OnLeave();
        Logic::CPlayPhase::GetInstance().OnLeave();
    }

    CPhase::Enum CPlayPhase::InternalOnRun()
    {
        Logic::CPlayPhase::GetInstance().OnRun();
        Gfx::CPlayPhase::GetInstance().OnRun();
        Gui::CPlayPhase::GetInstance().OnRun();

        return CPhase::Enum::PlayPhase;
    }
}
