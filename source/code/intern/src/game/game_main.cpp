#include "game/game_application.h"
#include "core/core_test.h"
#include "core/core_log.h"

#include <iostream>
#include <Windows.h>

#pragma warning(disable: 4100)

int APIENTRY WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ char*, _In_ int nShowCmd)
{
    Game::CApplication& rApplication = Game::CApplication::GetInstance();

    try
    {
        rApplication.Startup(1280, 720);
        rApplication.Run();
    }
    catch (std::exception& _rException)
    {
        Core::Logger::Log(_rException.what());
    }
    catch (...)
    {
        Core::Logger::Log("Startup/Run failed");
    }

    try
    {
        rApplication.Shutdown();
    }
    catch (std::exception& _rException)
    {
        Core::Logger::Log(_rException.what());
    }
    catch (...)
    {
        Core::Logger::Log("Shutdown failed");
    }

    std::cout << "Game ended!" << std::endl;
}
