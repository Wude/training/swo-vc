#pragma once

#include "core/core_singleton.h"
#include "game/game_phase.h"

namespace Game
{
    class CLoadMapPhase : public CPhase, public Core::CSingleton<CLoadMapPhase>
    {
        protected:

            void InternalOnEnter() override;
            void InternalOnLeave() override;
            CPhase::Enum InternalOnRun() override;

        private:

            template <class T> friend class Core::CSingleton;

        private:

            CLoadMapPhase();
            ~CLoadMapPhase() override;
    };
}
