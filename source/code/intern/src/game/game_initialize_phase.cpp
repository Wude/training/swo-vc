#include "game/game_initialize_phase.h"
#include "data/data_initialize_phase.h"
#include "logic/logic_initialize_phase.h"
#include "gfx/gfx_initialize_phase.h"
#include "gui/gui_initialize_phase.h"

namespace Game
{
    CInitializePhase::CInitializePhase()
    {
    }

    CInitializePhase::~CInitializePhase()
    {
    }

    void CInitializePhase::InternalOnEnter()
    {
        Data::CInitializePhase::GetInstance().OnEnter();
        Logic::CInitializePhase::GetInstance().OnEnter();
        Gfx::CInitializePhase::GetInstance().OnEnter();
        Gui::CInitializePhase::GetInstance().OnEnter();
    }

    void CInitializePhase::InternalOnLeave()
    {
        Gui::CInitializePhase::GetInstance().OnLeave();
        Gfx::CInitializePhase::GetInstance().OnLeave();
        Logic::CInitializePhase::GetInstance().OnLeave();
        Data::CInitializePhase::GetInstance().OnLeave();
    }

    CPhase::Enum CInitializePhase::InternalOnRun()
    {
        Data::CInitializePhase::GetInstance().OnRun();
        Logic::CInitializePhase::GetInstance().OnRun();
        Gfx::CInitializePhase::GetInstance().OnRun();
        Gui::CInitializePhase::GetInstance().OnRun();

        return CPhase::Enum::MainMenuPhase;
    }
}
