#include "game/game_application.h"
#include "game/game_unload_map_phase.h"
#include "data/data_unload_map_phase.h"
#include "logic/logic_unload_map_phase.h"
#include "gfx/gfx_unload_map_phase.h"
#include "gui/gui_unload_map_phase.h"

namespace Game
{
    CUnloadMapPhase::CUnloadMapPhase()
    {
    }

    CUnloadMapPhase::~CUnloadMapPhase()
    {
    }

    void CUnloadMapPhase::InternalOnEnter()
    {
        Data::CUnloadMapPhase::GetInstance().OnEnter();
        Logic::CUnloadMapPhase::GetInstance().OnEnter();
        Gfx::CUnloadMapPhase::GetInstance().OnEnter();
        Gui::CUnloadMapPhase::GetInstance().OnEnter();
    }

    void CUnloadMapPhase::InternalOnLeave()
    {
        Gui::CUnloadMapPhase::GetInstance().OnLeave();
        Gfx::CUnloadMapPhase::GetInstance().OnLeave();
        Logic::CUnloadMapPhase::GetInstance().OnLeave();
        Data::CUnloadMapPhase::GetInstance().OnLeave();
    }

    CPhase::Enum CUnloadMapPhase::InternalOnRun()
    {
        Data::CUnloadMapPhase::GetInstance().OnRun();
        Logic::CUnloadMapPhase::GetInstance().OnRun();
        Gfx::CUnloadMapPhase::GetInstance().OnRun();
        Gui::CUnloadMapPhase::GetInstance().OnRun();

        return CPhase::Enum::MainMenuPhase;
    }
}
