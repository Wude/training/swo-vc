
#pragma once

namespace Data
{
    class CEntity;
    class CEntityLink;
    class CEntityList;
    class CSector;
    class CMapSystem;
}

namespace Data
{
    class CEntityIterator
    {
        public:

            CEntityIterator();
            CEntityIterator(const CEntityIterator& _rOther);

        public:

            CEntity& operator * ();
            CEntity* operator -> ();

        private:

            CEntityIterator& operator++();
            CEntityIterator operator++(int);

        public:

            bool operator == (const CEntityIterator& _rOther);
            bool operator != (const CEntityIterator& _rOther);

        private:

            CEntityIterator(CEntityLink* _pLink);

        private:

            friend class CEntityList;
            friend class CSector;
            friend class CMapSystem;

        private:

            CEntityLink* m_pLink;
    };
}
