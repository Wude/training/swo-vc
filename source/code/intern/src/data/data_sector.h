#pragma once

#include "data/data_entity_category.h"
#include "data/data_entity_folder.h"
#include "core/core_aabb.h"

#include <cstddef>

namespace Data
{
    class CMapSystem;
}

namespace Data
{
    class CSector
    {
        private:

            static constexpr float AxisLength = 8.0f;

        private:

            CSector();

        private:

            void Initialize(int _Index, int _Row, int _Column, Core::CAABB2Float _AABB);

            int GetIndex();
            int GetRow();
            int GetColumn();
            Core::CAABB2Float& GetBoundingBox();
            CEntityFolder& GetFolder(const SEntityCategory::Enum _Category);

            void AddEntity(CEntity& _rEntity);
            void RemoveEntity(CEntity& _rEntity);

            CEntityIterator BeginEntity(int _CategoryIndex);
            CEntityIterator BeginEntityByCategory(const SEntityCategory::Enum _Category);
            CEntityIterator BeginEntityByAABB(const Core::CAABB2Float& _rAABB, int _CategoryIndex);
            CEntityIterator BeginEntityByAABBAndCategory(const Core::CAABB2Float& _rAABB, const SEntityCategory::Enum _Category);

        private:

            friend class CMapSystem;

        private:

            int               m_Index;
            int               m_Row;
            int               m_Column;
            Core::CAABB2Float m_AABB;
            CEntityFolder     m_Folders[SEntityCategory::NumberOfCategories];
    };
}
