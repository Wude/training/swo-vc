#include "data/data_entity_iterator.h"
#include "data/data_entity_link.h"

#include <assert.h>

namespace Data
{
    CEntityIterator::CEntityIterator()
        : m_pLink(nullptr)
    {
    }

    // -----------------------------------------------------------------------------

    CEntityIterator::CEntityIterator(const CEntityIterator& _rOther)
        : m_pLink(_rOther.m_pLink)
    {
    }

    // -----------------------------------------------------------------------------

    CEntityIterator::CEntityIterator(CEntityLink* _pLink)
        : m_pLink(_pLink)
    {
    }

    // -----------------------------------------------------------------------------

    CEntity& CEntityIterator::operator * ()
    {
        assert(m_pLink != nullptr);

        return m_pLink->GetEntity();
    }

    // -----------------------------------------------------------------------------

    CEntity* CEntityIterator::operator -> ()
    {
        assert(m_pLink != nullptr);

        return &m_pLink->GetEntity();
    }

    // -----------------------------------------------------------------------------

    CEntityIterator& CEntityIterator::operator++()
    {
        assert(m_pLink != nullptr);

        m_pLink = m_pLink->GetNext();
        return *this;
    }

    CEntityIterator CEntityIterator::operator++(int)
    {
        assert(m_pLink != nullptr);

        CEntityIterator copy = CEntityIterator(m_pLink);
        ++(*this);
        return copy;
    }

    // -----------------------------------------------------------------------------

    bool CEntityIterator::operator == (const CEntityIterator& _rOther)
    {
        return m_pLink == _rOther.m_pLink;
    }

    bool CEntityIterator::operator != (const CEntityIterator& _rOther)
    {
        return m_pLink != _rOther.m_pLink;
    }

}
