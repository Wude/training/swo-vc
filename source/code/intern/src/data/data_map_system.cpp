#include "data/data_map_system.h"

namespace Data
{
    static const CEntityIterator s_EndIterator = CEntityIterator();

    CMapSystem::CMapSystem()
        : m_Width{ 0 }
        , m_Height{ 0 }
        , m_SectorRowCount{ 0 }
        , m_SectorColumnCount{ 0 }
        , m_SectorCount{ 0 }
        , m_pSectors{ nullptr }
    {
    }

    CMapSystem::~CMapSystem()
    {
        DestroyMap();
    }

    void CMapSystem::CreateMap(float _MinWidth, float _MinHeight)
    {
        DestroyMap();

        m_SectorRowCount = static_cast<int>(std::ceilf(_MinWidth / CSector::AxisLength));
        m_SectorColumnCount = static_cast<int>(std::ceilf(_MinHeight / CSector::AxisLength));
        m_SectorCount = m_SectorRowCount * m_SectorColumnCount;
        m_Width = m_SectorRowCount * CSector::AxisLength;
        m_Height = m_SectorColumnCount * CSector::AxisLength;

        CSector* pSectors = new CSector[static_cast<size_t>(m_SectorRowCount) * static_cast<size_t>(m_SectorColumnCount)];

        if (pSectors != nullptr)
        {
            m_pSectors = pSectors;

            for (int Row = 0; Row < m_SectorRowCount; ++ Row)
            {
                for (int Column = 0; Column < m_SectorColumnCount; ++ Column)
                {
                    float X1 = CSector::AxisLength * Row;
                    float Y1 = CSector::AxisLength * Column;
                    float X2 = CSector::AxisLength + X1;
                    float Y2 = CSector::AxisLength + Y1;

                    int Index = GetSectorIndex(Row, Column);
                    m_pSectors[Index].Initialize(Index, Row, Column, Core::CAABB2Float(X1, Y1, X2, Y2));
                }
            }
        }
    }

    void CMapSystem::DestroyMap()
    {
        if (m_pSectors != nullptr)
        {
            delete[] m_pSectors;
            m_pSectors = nullptr;
        }
    }

    void CMapSystem::AddEntity(CEntity& _rEntity)
    {
        const Core::Float2 Point = _rEntity.GetBoundingBox().GetCenter();

        int Row = static_cast<int>(std::floorf(m_Height / Point[0]));
        int Column = static_cast<int>(std::floorf(m_Width / Point[1]));

        m_pSectors[GetSectorIndex(Row, Column)].AddEntity(_rEntity);
    }

    void CMapSystem::RemoveEntity(CEntity& _rEntity)
    {
        CEntityFolder* pFolder = _rEntity.GetFolder();

        if (pFolder != nullptr)
        {
            pFolder->GetSector().RemoveEntity(_rEntity);
        }
    }

    int CMapSystem::GetSectorIndex(const int _Row, const int _Column) const
    {
        return m_SectorColumnCount * _Row + _Column;
    }

    void CMapSystem::GetSectorBounds(const Core::CAABB2Float& _rAABB, int& _rMinRow, int& _rMinColumn, int& _rMaxRow, int& _rMaxColumn) const
    {
        const Core::Float2 P1 = _rAABB.GetMin();
        const Core::Float2 P2 = _rAABB.GetMax();

        _rMinRow    = Core::Clamp(static_cast<int>(std::floorf(P1[0] / CSector::AxisLength)), 0, m_SectorRowCount - 1);
        _rMinColumn = Core::Clamp(static_cast<int>(std::floorf(P1[1] / CSector::AxisLength)), 0, m_SectorColumnCount - 1);

        _rMaxRow    = Core::Clamp(static_cast<int>(std::ceilf(P2[0] / CSector::AxisLength)), 0, m_SectorRowCount - 1);
        _rMaxColumn = Core::Clamp(static_cast<int>(std::ceilf(P2[1] / CSector::AxisLength)), 0, m_SectorColumnCount - 1);
    }

    CEntityIterator CMapSystem::BeginEntity()
    {
        for (int SectorIndex = 0; SectorIndex < m_SectorCount; ++ SectorIndex)
        {
            CEntityIterator WhereNext = m_pSectors[SectorIndex].BeginEntity(0);
            if (WhereNext != s_EndIterator)
            {
                return WhereNext;
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CMapSystem::NextEntity(CEntityIterator _Where)
    {
        CEntityIterator WhereNext = _Where ++;
        if (WhereNext != s_EndIterator)
        {
            return WhereNext;
        }

        CEntityFolder* pFolder = _Where->GetFolder();
        if (pFolder != nullptr)
        {
            CSector& rSector = pFolder->GetSector();
            WhereNext = rSector.BeginEntity(_Where->GetCategory() + 1);
            if (WhereNext != s_EndIterator)
            {
                return WhereNext;
            }

            for (int SectorIndex = rSector.GetIndex() + 1; SectorIndex < m_SectorCount; ++ SectorIndex)
            {
                WhereNext = m_pSectors[SectorIndex].BeginEntity(0);
                if (WhereNext != s_EndIterator)
                {
                    return WhereNext;
                }
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CMapSystem::BeginEntity(const SEntityCategory::Enum _Category)
    {
        for (int SectorIndex = 0; SectorIndex < m_SectorCount; ++ SectorIndex)
        {
            CEntityIterator WhereNext = m_pSectors[SectorIndex].BeginEntityByCategory(_Category);
            if (WhereNext != s_EndIterator)
            {
                return WhereNext;
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CMapSystem::NextEntity(CEntityIterator _Where, const SEntityCategory::Enum _Category)
    {
        CEntityIterator WhereNext = _Where ++;
        if (WhereNext != s_EndIterator)
        {
            return WhereNext;
        }

        CEntityFolder* pFolder = _Where->GetFolder();
        if (pFolder != nullptr)
        {
            for (int SectorIndex = pFolder->GetSector().GetIndex() + 1; SectorIndex < m_SectorCount; ++ SectorIndex)
            {
                WhereNext = m_pSectors[SectorIndex].BeginEntityByCategory(_Category);
                if (WhereNext != s_EndIterator)
                {
                    return WhereNext;
                }
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CMapSystem::BeginEntity(const Core::CAABB2Float& _rAABB)
    {
        int MinRow    = 0;
        int MinColumn = 0;
        int MaxRow    = 0;
        int MaxColumn = 0;
        GetSectorBounds(_rAABB, MinRow, MinColumn, MaxRow, MaxColumn);

        for (int Row = MinRow; Row <= MaxRow; ++ Row)
        {
            for (int Column = MinColumn; Column <= MaxColumn; ++ Column)
            {
                CEntityIterator WhereNext = m_pSectors[GetSectorIndex(Row, Column)].BeginEntityByAABB(_rAABB, 0);
                if (WhereNext != s_EndIterator)
                {
                    return WhereNext;
                }
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CMapSystem::NextEntity(CEntityIterator _Where, const Core::CAABB2Float& _rAABB)
    {
        CEntityIterator WhereNext;
        for (WhereNext = _Where ++; WhereNext != s_EndIterator; ++ WhereNext)
        {
            if (WhereNext->GetBoundingBox().Intersects(_rAABB))
            {
                return WhereNext;
            }
        }

        CEntityFolder* pFolder = _Where->GetFolder();
        if (pFolder != nullptr)
        {
            CSector& rSector = pFolder->GetSector();
            WhereNext = rSector.BeginEntityByAABB(_rAABB, _Where->GetCategory() + 1);
            if (WhereNext != s_EndIterator)
            {
                return WhereNext;
            }

            int Row       = 0;
            int MinColumn = 0;
            int MaxRow    = 0;
            int MaxColumn = 0;
            GetSectorBounds(_rAABB, Row, MinColumn, MaxRow, MaxColumn);

            Row = rSector.GetRow();
            for (int Column = rSector.GetColumn() + 1; Column <= MaxColumn; ++ Column)
            {
                WhereNext = m_pSectors[GetSectorIndex(Row, Column)].BeginEntityByAABB(_rAABB, 0);
                if (WhereNext != s_EndIterator)
                {
                    return WhereNext;
                }
            }

            for (++ Row; Row <= MaxRow; ++ Row)
            {
                for (int Column = MinColumn; Column <= MaxColumn; ++ Column)
                {
                    WhereNext = m_pSectors[GetSectorIndex(Row, Column)].BeginEntityByAABB(_rAABB, 0);
                    if (WhereNext != s_EndIterator)
                    {
                        return WhereNext;
                    }
                }
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CMapSystem::BeginEntity(const Core::CAABB2Float& _rAABB, const SEntityCategory::Enum _Category)
    {
        int MinRow    = 0;
        int MinColumn = 0;
        int MaxRow    = 0;
        int MaxColumn = 0;
        GetSectorBounds(_rAABB, MinRow, MinColumn, MaxRow, MaxColumn);

        for (int Row = MinRow; Row <= MaxRow; ++ Row)
        {
            for (int Column = MinColumn; Column <= MaxColumn; ++ Column)
            {
                CEntityIterator WhereNext = m_pSectors[GetSectorIndex(Row, Column)].BeginEntityByAABBAndCategory(_rAABB, _Category);
                if (WhereNext != s_EndIterator)
                {
                    return WhereNext;
                }
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CMapSystem::NextEntity(CEntityIterator _Where, const Core::CAABB2Float& _rAABB, const SEntityCategory::Enum _Category)
    {
        CEntityIterator WhereNext;
        for (WhereNext = _Where ++; WhereNext != s_EndIterator; ++ WhereNext)
        {
            if (WhereNext->GetBoundingBox().Intersects(_rAABB))
            {
                return WhereNext;
            }
        }

        CEntityFolder* pFolder = _Where->GetFolder();
        if (pFolder != nullptr)
        {
            CSector& rSector = pFolder->GetSector();

            int Row       = 0;
            int MinColumn = 0;
            int MaxRow    = 0;
            int MaxColumn = 0;
            GetSectorBounds(_rAABB, Row, MinColumn, MaxRow, MaxColumn);

            Row = rSector.GetRow();
            for (int Column = rSector.GetColumn() + 1; Column <= MaxColumn; ++ Column)
            {
                WhereNext = m_pSectors[GetSectorIndex(Row, Column)].BeginEntityByAABBAndCategory(_rAABB, _Category);
                if (WhereNext != s_EndIterator)
                {
                    return WhereNext;
                }
            }

            for (++ Row; Row <= MaxRow; ++ Row)
            {
                for (int Column = MinColumn; Column <= MaxColumn; ++ Column)
                {
                    WhereNext = m_pSectors[GetSectorIndex(Row, Column)].BeginEntityByAABBAndCategory(_rAABB, _Category);
                    if (WhereNext != s_EndIterator)
                    {
                        return WhereNext;
                    }
                }
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CMapSystem::EndEntity()
    {
        return s_EndIterator;
    }
}
