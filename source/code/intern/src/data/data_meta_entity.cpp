#include "data/data_meta_entity.h"

namespace Data
{
    CMetaEntity::CMetaEntity()
        : m_Facets{ }
        , m_Category{ SEntityCategory::Enum::Undefined }
    {
    }

    CMetaEntity::~CMetaEntity()
    {
    }

    const SEntityCategory::Enum& CMetaEntity::GetCategory() const
    {
        return m_Category;
    }

    CMetaEntity::BID CMetaEntity::GetID()
    {
        return m_ID;
    }

    void CMetaEntity::SetID(CMetaEntity::BID _ID)
    {
        m_ID = _ID;
    }

    void CMetaEntity::SetFacet(SEntityFacet::Enum _Facet, void* _pFacet)
    {
        m_Facets[_Facet] = _pFacet;
    }

    void* CMetaEntity::GetFacet(SEntityFacet::Enum _Facet)
    {
        return m_Facets[_Facet];
    }

    void CMetaEntity::Initialize(Core::Float2 _Size)
    {
        m_Size = _Size;
    }

    Core::Float2 CMetaEntity::GetSize() const
    {
        return m_Size;
    }
}
