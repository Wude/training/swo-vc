#pragma once

#include "core/core_vector.h"
#include "core/core_id_manager.h"
#include "data/data_entity_facet.h"
#include "data/data_entity_category.h"

#include <array>

namespace Data
{
    class CMetaEntitySystem;
}

namespace Data
{
    class CMetaEntity
    {
        public:

            using BID = Core::CIDManager::BID;

        public:

            const SEntityCategory::Enum& GetCategory() const;

            CMetaEntity::BID GetID();

            void SetFacet(SEntityFacet::Enum _Facet, void* _pFacet);
            void* GetFacet(SEntityFacet::Enum _Facet);

            Core::Float2 GetSize() const;

        public:

            CMetaEntity();
           ~CMetaEntity();

        private:

            friend class CMetaEntitySystem;

        private:

            void Initialize(Core::Float2 _Size);
            void SetID(CMetaEntity::BID _ID);

        private:

            using CFacetArray = std::array<void*, SEntityFacet::Enum::NumberOfFacets>;

        private:

            CMetaEntity::BID      m_ID;
            CFacetArray           m_Facets;
            Core::Float2          m_Size;
            SEntityCategory::Enum m_Category;
    };
}
