#include "data/data_entity.h"

namespace Data
{
    CEntity::CEntity()
        : m_Facets{ }
        , m_pFolder{ nullptr }
        , m_pMetaEntity{ nullptr }
    {
    }

    CEntity::~CEntity()
    {
    }

    SEntityCategory::Enum CEntity::GetCategory() const
    {
        return m_pMetaEntity->GetCategory();
    }

    CEntity::BID CEntity::GetID()
    {
        return m_ID;
    }

    void CEntity::SetID(CEntity::BID _ID)
    {
        m_ID = _ID;
    }

    CEntityFolder* CEntity::GetFolder()
    {
        return m_pFolder;
    }

    void CEntity::SetFolder(CEntityFolder* _pFolder)
    {
        m_pFolder = _pFolder;
    }

    void CEntity::SetFacet(SEntityFacet::Enum _Facet, void* _pFacet)
    {
        m_Facets[_Facet] = _pFacet;
    }

    void* CEntity::GetFacet(SEntityFacet::Enum _Facet)
    {
        return m_Facets[_Facet];
    }

    void CEntity::Initialize(CMetaEntity& _rMetaEntity, Core::Float2 _Position)
    {
        m_pMetaEntity = &_rMetaEntity;
        m_Position    = _Position;
    }

    const Core::CAABB2Float& CEntity::GetBoundingBox() const
    {
        return m_AABB;
    }
}
