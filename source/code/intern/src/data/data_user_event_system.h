#pragma once

#include "data/data_event_system.h"
#include "core/core_user_event.h"

namespace Data
{
    class CUserEventSystem : public CEventSystem<Core::CUserEvent>
    {
        private:

           ~CUserEventSystem() override;
    };
}
