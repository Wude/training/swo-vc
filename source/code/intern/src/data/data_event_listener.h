#pragma once

namespace Data
{
    template <class T>
    class CEventListener
    {
        public:

            virtual void OnEvent(const T& _rEvent) = 0;
    };
}
