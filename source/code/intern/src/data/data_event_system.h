#pragma once

#include "core/core_singleton.h"
#include "data/data_event_listener.h"

#include <list>
#include <vector>
#include <algorithm>

namespace Data
{
    template <class T>
    class CEventSystem : public Core::CSingleton<CEventSystem<T>>
    {
        public:

            void Register(CEventListener<T>& _rListener)
            {
                if (!(std::find(m_EventListeners.begin(), m_EventListeners.end(), &_rListener) != m_EventListeners.end()))
                {
                    m_EventListeners.push_back(&_rListener);
                }
            }

            void Unregister(CEventListener<T>& _rListener)
            {
                m_EventListeners.remove(&_rListener);
            }

            void FireEvent(const T& _rEvent)
            {
                CEventListener<T>* pEventListener;

                for (typename CEventListenerList::const_iterator Iterator = m_EventListeners.begin(); Iterator != m_EventListeners.end(); ++ Iterator)
                {
                    pEventListener = *Iterator;

                    if (pEventListener != nullptr)
                    {
                        pEventListener->OnEvent(_rEvent);
                    }
                }
            }

        private:

            template <class T> friend class Core::CSingleton;

            using CEventListenerList = std::list<CEventListener<T>*>;

        protected:

            CEventSystem()
                : m_EventListeners{ }
            {
            }

            virtual ~CEventSystem()
            {
            }

        private:

            CEventListenerList m_EventListeners;
    };
}
