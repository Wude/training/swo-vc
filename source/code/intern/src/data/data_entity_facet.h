#pragma once

namespace Data
{
    struct SEntityFacet
    {
        enum Enum : int
        {
            GraphicsFacet,
            LogicFacet,
            NumberOfFacets
        };
    };
}
