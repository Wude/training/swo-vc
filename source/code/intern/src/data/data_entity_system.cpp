#include "data/data_entity_system.h"

#include <algorithm>

namespace Data
{
    CEntitySystem::CEntitySystem()
        : m_Entities{}
    {
    }

    CEntitySystem::~CEntitySystem()
    {
    }

    CEntity& CEntitySystem::CreateEntity(const std::string& _rName)
    {
        Core::CIDManager::BID ID = m_IDManager.Register(_rName);
        CEntity& rEntity = m_ItemManager.CreateItem(ID);
        rEntity.SetID(ID);
        m_Entities.push_back(&rEntity);
        return rEntity;
    }

    void CEntitySystem::DestroyEntity(CEntity& _rEntity)
    {
        m_Entities.remove(&_rEntity);
    }
}
