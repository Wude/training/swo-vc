#pragma once

#include "core/core_singleton.h"
#include "core/core_aabb.h"
#include "data/data_entity_category.h"
#include "data/data_entity.h"
#include "data/data_entity_iterator.h"
#include "data/data_sector.h"

#include <cstddef>

namespace Data
{
    class CMapSystem : public Core::CSingleton<CMapSystem>
    {
        public:

            void CreateMap(float _MinWidth, float _MinHeight);
            void DestroyMap();

            void AddEntity(CEntity& _rEntity);
            void RemoveEntity(CEntity& _rEntity);

            CEntityIterator BeginEntity();
            CEntityIterator NextEntity(CEntityIterator _Where);

            CEntityIterator BeginEntity(const SEntityCategory::Enum _Category);
            CEntityIterator NextEntity(CEntityIterator _Where, SEntityCategory::Enum _Category);

            CEntityIterator BeginEntity(const Core::CAABB2Float& _rAABB);
            CEntityIterator NextEntity(CEntityIterator _Where, const Core::CAABB2Float& _rAABB);

            CEntityIterator BeginEntity(const Core::CAABB2Float& _rAABB, const SEntityCategory::Enum _Category);
            CEntityIterator NextEntity(CEntityIterator _Where, const Core::CAABB2Float& _rAABB, SEntityCategory::Enum _Category);

            CEntityIterator EndEntity();

        private:

            template <class T> friend class Core::CSingleton;

        private:

            int GetSectorIndex(const int _Row, const int _Column) const;
            void GetSectorBounds(const Core::CAABB2Float& _rAABB, int& _rMinRow, int& _rMinColumn, int& _rMaxRow, int& _rMaxColumn) const;

        private:

            CMapSystem();
            ~CMapSystem();

        private:

            float    m_Width;
            float    m_Height;
            int      m_SectorRowCount;
            int      m_SectorColumnCount;
            int      m_SectorCount;
            CSector* m_pSectors;
    };
}
