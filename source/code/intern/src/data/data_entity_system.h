#pragma once

#include "core/core_singleton.h"
#include "core/core_id_manager.h"
#include "core/core_item_manager.h"
#include "data/data_entity.h"

#include <vector>
#include <string>

namespace Data
{
    class CEntitySystem : public Core::CSingleton<CEntitySystem>
    {
        public:

            CEntity& CreateEntity(const std::string& _rName);
            void DestroyEntity(CEntity& _rEntity);

        private:

            CEntitySystem();
           ~CEntitySystem();

        private:

            Core::CIDManager            m_IDManager;
            Core::CItemManager<CEntity> m_ItemManager;
            std::list<CEntity*>         m_Entities;
    };
}
