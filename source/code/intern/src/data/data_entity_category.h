#pragma once

namespace Data
{
    struct SEntityCategory
    {
        enum Enum : int
        {
            Character,
            Ground,
            Obstacle,
            NumberOfCategories,
            Undefined = -1,
        };
    };
}
