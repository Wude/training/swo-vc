#pragma once

#include "data/data_entity_category.h"
#include "data/data_entity.h"
#include "data/data_entity_list.h"

namespace Data
{
    class CSector;
    class CMapSystem;
}

namespace Data
{
    class CEntityFolder
    {
        public:

            CEntityFolder();
           ~CEntityFolder();

        public:

            CEntityList &GetEntities();
            void AddEntity(CEntity& _rEntity);
            void RemoveEntity(CEntity& _rEntity);

        private:

            friend class CSector;
            friend class CMapSystem;

        private:

            CSector& GetSector();
            void SetSector(CSector& _rSector);

        private:

            CEntityList m_Entities;
            CSector*    m_pSector;
    };
}
