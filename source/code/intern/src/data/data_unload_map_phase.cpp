#include "data/data_unload_map_phase.h"
#include "data/data_map_system.h"

namespace Data
{
    void CUnloadMapPhase::OnEnter()
    {
    }

    void CUnloadMapPhase::OnRun()
    {
        CMapSystem::GetInstance().DestroyMap();
    }

    void CUnloadMapPhase::OnLeave()
    {
    }
}
