#pragma once

#include "core/core_singleton.h"
#include "core/core_id_manager.h"
#include "core/core_item_manager.h"
#include "data/data_entity.h"

#include <vector>
#include <string>

namespace Data
{
    class CMetaEntitySystem : public Core::CSingleton<CMetaEntitySystem>
    {
        public:

            CMetaEntity& CreateMetaEntity(const std::string& _rName);
            void DestroyMetaEntity(CMetaEntity& _rMetaEntity);

        private:

            CMetaEntitySystem();
           ~CMetaEntitySystem();

        private:

            Core::CIDManager                m_IDManager;
            Core::CItemManager<CMetaEntity> m_ItemManager;
            std::list<CMetaEntity*>         m_MetaEntities;
    };
}
