#include "data/data_meta_entity_system.h"

#include <algorithm>

namespace Data
{
    CMetaEntitySystem::CMetaEntitySystem()
        : m_MetaEntities{}
    {
    }

    CMetaEntitySystem::~CMetaEntitySystem()
    {
    }

    CMetaEntity& CMetaEntitySystem::CreateMetaEntity(const std::string& _rName)
    {
        Core::CIDManager::BID ID = m_IDManager.Register(_rName);
        CMetaEntity& rMetaEntity = m_ItemManager.CreateItem(ID);
        rMetaEntity.SetID(ID);
        m_MetaEntities.push_back(&rMetaEntity);
        return rMetaEntity;
    }

    void CMetaEntitySystem::DestroyMetaEntity(CMetaEntity& _rMetaEntity)
    {
        m_MetaEntities.remove(&_rMetaEntity);
    }
}
