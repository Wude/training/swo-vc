#include "data/data_sector.h"

namespace Data
{
    static const CEntityIterator s_EndIterator = CEntityIterator();

    CSector::CSector()
        : m_Index  { 0 }
        , m_Row    { 0 }
        , m_Column { 0 }
        , m_AABB   { }
        , m_Folders{ }
    {
        for (int Index = 0; Index < SEntityCategory::NumberOfCategories; ++ Index)
        {
            m_Folders[Index].SetSector(*this);
        }
    }

    void CSector::Initialize(int _Index, int _Row, int _Column, Core::CAABB2Float _AABB)
    {
        m_Index  = _Index;
        m_Row    = _Row;
        m_Column = _Column;
        m_AABB   = _AABB;
    }

    int CSector::GetIndex()
    {
        return m_Index;
    }

    int CSector::GetRow()
    {
        return m_Row;
    }

    int CSector::GetColumn()
    {
        return m_Column;
    }

    Core::CAABB2Float& CSector::GetBoundingBox()
    {
        return m_AABB;
    }

    CEntityFolder& CSector::GetFolder(const SEntityCategory::Enum _Category)
    {
        return m_Folders[_Category];
    }

    void CSector::AddEntity(CEntity& _rEntity)
    {
        m_Folders[_rEntity.GetCategory()].AddEntity(_rEntity);
    }

    void CSector::RemoveEntity(CEntity& _rEntity)
    {
        m_Folders[_rEntity.GetCategory()].RemoveEntity(_rEntity);
    }

    CEntityIterator CSector::BeginEntity(int _CategoryIndex)
    {
        for (; _CategoryIndex < SEntityCategory::Enum::NumberOfCategories; ++ _CategoryIndex)
        {
            CEntityIterator WhereNext = m_Folders[_CategoryIndex].GetEntities().Begin();
            if (WhereNext != s_EndIterator)
            {
                return WhereNext;
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CSector::BeginEntityByCategory(const SEntityCategory::Enum _Category)
    {
        CEntityIterator WhereNext = m_Folders[_Category].GetEntities().Begin();
        if (WhereNext != s_EndIterator)
        {
            return WhereNext;
        }
        return s_EndIterator;
    }

    CEntityIterator CSector::BeginEntityByAABB(const Core::CAABB2Float& _rAABB, int _CategoryIndex)
    {
        for (; _CategoryIndex < SEntityCategory::Enum::NumberOfCategories; ++ _CategoryIndex)
        {
            for (CEntityIterator WhereNext = m_Folders[_CategoryIndex].GetEntities().Begin(); WhereNext != s_EndIterator; ++ WhereNext)
            {
                if (WhereNext->GetBoundingBox().Intersects(_rAABB))
                {
                    return WhereNext;
                }
            }
        }
        return s_EndIterator;
    }

    CEntityIterator CSector::BeginEntityByAABBAndCategory(const Core::CAABB2Float& _rAABB, const SEntityCategory::Enum _Category)
    {
        for (CEntityIterator WhereNext = m_Folders[_Category].GetEntities().Begin(); WhereNext != s_EndIterator; ++ WhereNext)
        {
            if (WhereNext->GetBoundingBox().Intersects(_rAABB))
            {
                return WhereNext;
            }
        }
        return s_EndIterator;
    }
}
