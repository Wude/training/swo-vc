#pragma once

#include "data/data_meta_entity.h"
#include "data/data_entity_facet.h"
#include "data/data_entity_link.h"
#include "core/core_aabb.h"
#include "core/core_vector.h"

#include <array>

namespace Data
{
    class CEntityList;
    class CEntityFolder;
    class CEntitySystem;
    class CMapSystem;
}

namespace Data
{
    class CEntity
    {
        public:

            using BID = Core::CIDManager::BID;

        public:

            SEntityCategory::Enum GetCategory() const;

            CEntity::BID GetID();

            void SetFacet(SEntityFacet::Enum _Facet, void* _pFacet);
            void* GetFacet(SEntityFacet::Enum _Facet);

            const Core::CAABB2Float& GetBoundingBox() const;

        public:

            CEntity();
           ~CEntity();

        private:
            
            friend class CEntityFolder;
            friend class CEntitySystem;
            friend class CMapSystem;

        private:

            void Initialize(CMetaEntity& _rMetaEntity, Core::Float2 _Position);
            void SetID(CEntity::BID _ID);

            CEntityFolder* GetFolder();
            void SetFolder(CEntityFolder* _pFolder);

        private:

            using CFacetArray = std::array<void*, SEntityFacet::Enum::NumberOfFacets>;

        private:

            CEntity::BID        m_ID;
            CFacetArray         m_Facets;
            Core::Float2        m_Position;
            Core::CAABB2Float   m_AABB;
            CEntityLink         m_Link;
            CEntityFolder*      m_pFolder;
            CMetaEntity*        m_pMetaEntity;

        private:

            friend class CEntityLink;
            friend class CEntityList;
    };
}
