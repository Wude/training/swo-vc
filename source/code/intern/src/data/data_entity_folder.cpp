#include "data/data_entity_folder.h"

namespace Data
{
    CEntityFolder::CEntityFolder()
        : m_pSector{ nullptr }
    {
    }

    CEntityFolder::~CEntityFolder()
    {
        m_Entities.Clear();
    }

    CEntityList &CEntityFolder::GetEntities()
    {
        return m_Entities;
    }

    void CEntityFolder::AddEntity(CEntity& _rEntity)
    {
        m_Entities.PushBack(_rEntity);
        _rEntity.SetFolder(this);
    }

    void CEntityFolder::RemoveEntity(CEntity& _rEntity)
    {
        m_Entities.Erase(_rEntity);
        _rEntity.SetFolder(nullptr);
    }

    CSector& CEntityFolder::GetSector()
    {
        assert(m_pSector != nullptr);
        return *m_pSector;
    }

    void CEntityFolder::SetSector(CSector& _rSector)
    {
        m_pSector = &_rSector;
    }
}
